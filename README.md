# CDA Conceptual Project

![GitHub version](https://d25lcipzij17d.cloudfront.net/badge.svg?id=gh&r=r&type=6e&v=1.0.0&x2=0)



_This app will be about ..._
1. targeting sites such as discord channels & Dealabs & amazon homemade api 
products prices & compare them to the history ones for error prices & good 
deals (not sure yet)
2. developer web scraper to centralize daily news, security & actualities, CVE, etc..


_both are meant for private/personal uses_

### Features

- web scraping (rss feeds & local api)
- desktop app (electron)
- discord bot & api (websocket & gateway)
- mobile instant & real time push notifications (app?)

## REAC

![](./assets/REAC_CDA_V03.png)

## Tech

- React // Vue // Angular + */Typescript
- Electron
- Symfony // NodeJS
- React Native // Flutter
- Expo // Workbox
- Docker
- Bootstrap
- ...

## Diagram

![](./assets/CDA_diagram-project.png)

## Use Cases

![](./assets/CDA_use-case.png)

## Figma Mockups

![](./assets/CDA_project-mockups-vectors.png)

## CDM

![](./assets/CDA_project-CDM.png)

## _More to come..!_
_todo => Prototype & charts_